# FROM ubuntu:18.04
#  install  jdk packages
# RUN apt-get update && \
#     apt-get install -y curl \
#     wget \
#     openjdk-8-jdk && \
#     rm -rf /var/lib/apt/lists/* 

# WORKDIR /app
# COPY MaharMPT /app
# EXPOSE 8180
# CMD [ "/app/bin/MaharMPT" , "/bin/bash" ]

FROM alpine:3.12.0
RUN apk add openjdk8
WORKDIR /app
COPY MaharMPT /app
EXPOSE 8180
CMD [ "/app/bin/MaharMPT" , "/bin/sh" ]